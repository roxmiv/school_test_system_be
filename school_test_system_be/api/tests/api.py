from datetime import datetime, timedelta
from django.utils import timezone
from json_api.commands import Error
from json_api.decorators import post_api_request, get_api_request
from school_test_system_be.common_settings import DATETIME_FORMAT
from school_test_system_be.settings import SCORE_SCALE
from tasks.models import Subject, Topic, Question, QuestionAnswer, MaskTask, Task
from tests.models import Test, TestTask, Variant, VariantTask, StudentTest, VariantTaskAnswer
from users_auth.decorators import login_required, school_admin_required, student_required, teacher_required
from users_auth.models import MainUser, Student, GradeDifficulty, Grade


def get_mask_task(task):
    try:
        mask_task = MaskTask.objects.get(task=task, date_time__gte=task.date_time)
        return mask_task
    except MaskTask.DoesNotExist:
        mask_task = MaskTask.objects.create(task=task,
                                            text=task.text,
                                            topic=task.topic,
                                            author=task.author,
                                            is_training=task.is_training,
                                            is_control=task.is_control)
        for grade_difficulty in task.grade_difficulty.all():
            grade_difficulty.mask_tasks.add(mask_task)
            grade_difficulty.save()
        for question in task.questions.all():
            new_question = Question(text=question.text,
                                    ans_type=question.ans_type,
                                    mask_task=mask_task,
                                    position=question.position)
            new_question.save()
            for answer in question.answers.all():
                QuestionAnswer(text=answer.text,
                               is_correct=answer.is_correct,
                               question=new_question).save()
        return mask_task
        

def tasks_generation(test, is_control):
    test_tasks = TestTask.objects.filter(test=test).order_by('position')
    num_topics = dict()
    for test_task in test_tasks:
        if (test_task.topic, test_task.grade_difficulty) in num_topics:
            num_topics[(test_task.topic, test_task.grade_difficulty)] += 1
        else:
            num_topics[(test_task.topic, test_task.grade_difficulty)] = 1
    tasks_with_topics = dict()
    for topic, grade_difficulty in num_topics:
        if is_control:
            if Task.objects.filter(topic=topic, grade_difficulty=grade_difficulty, is_control=True).count() < num_topics[(topic, grade_difficulty)]:
                raise Error(response_code=409,
                            text="Недостаточно задач для темы " + topic.name + " со сложностью " + str(grade_difficulty.difficulty) + ".")
            tasks_with_topics[(topic, grade_difficulty)] = list(Task.objects.filter(topic=topic,
                                                                                    grade_difficulty=grade_difficulty,
                                                                                    is_control=True).distinct().order_by('?'))[:num_topics[(topic, grade_difficulty)]]
        else:
            if Task.objects.filter(topic=topic, grade_difficulty=grade_difficulty, is_training=True).count() < num_topics[(topic, grade_difficulty)]:
                raise Error(response_code=409,
                            text="Недостаточно тренировочных задач для темы " + topic.name + " со сложностью " + str(grade_difficulty.difficulty) + ".")
            tasks_with_topics[(topic, grade_difficulty)] = list(Task.objects.filter(topic=topic,
                                                                                    grade_difficulty=grade_difficulty,
                                                                                    is_training=True).distinct().order_by('?'))[:num_topics[(topic, grade_difficulty)]]
    tasks = []
    for test_task in test_tasks:
        topic = test_task.topic
        grade_difficulty = test_task.grade_difficulty
        num_topics[(topic, grade_difficulty)] -= 1
        tasks.append(get_mask_task(tasks_with_topics[(topic, grade_difficulty)][num_topics[(topic, grade_difficulty)]]))
    return tasks


def create_variant(student_test, student, mask_tasks, is_control):
    new_variant = Variant.objects.create(student_test=student_test,
                                         student=student,
                                         is_control=is_control)
    if is_control:
        new_variant.deadline = student_test.deadline
    else:
        new_variant.deadline = student_test.training_deadline
    if student_test.test.duration is not None:
        new_variant.deadline = min(timezone.now() + student_test.test.duration, new_variant.deadline)
    i = 1
    for mask_task in mask_tasks:
        VariantTask(mask_task=mask_task, position=i, variant=new_variant).save()
        i += 1
    new_variant.save()
    student_test.num_attemps -= 1
    student_test.save()
    return new_variant


@post_api_request
@login_required
@student_required
def test_generation(request, data):
    try:
        student_test = StudentTest.objects.get(id=data['student_test']['id'])
        student = request.main_user.student
        if student.id != student_test.student.id:
            raise Error(response_code=409, text="Пользователю не назначен этот тест.")
        now = timezone.now()
        if now < student_test.starttime:
            raise Error(response_code=409, text="Тест пока еще нельзя сдавать.")
        if now > student_test.deadline:
            raise Error(response_code=409, text="Срок сдачи теста прошел.")
        if data['is_control'] == False and student_test.training_starttime is None:
            raise Error(response_code=409, text="Тренировочные тесты не доступны.")
        if data['is_control'] == True and student_test.num_attemps == 0:
            raise Error(response_code=409, text="У пользователя закончились попытки на этот тест.")
        tasks = tasks_generation(student_test.test, data['is_control'])
        return create_variant(student_test, student, tasks, data['is_control']).serialize()
    except StudentTest.DoesNotExist:
        raise Error(response_code=409, text="Такого теста не существует.")


def training_tasks_generation(test_tasks, num_topics):
    tasks_with_topics = dict()
    for topic, grade_difficulty in num_topics:
        if Task.objects.filter(topic=topic, grade_difficulty=grade_difficulty, is_training=True).count() < num_topics[(topic, grade_difficulty)]:
            raise Error(response_code=409,
                        text="Недостаточно тренировочных задач для темы " + topic.name + " со сложностью " + str(grade_difficulty.difficulty) + ".")
        tasks_with_topics[(topic, grade_difficulty)] = list(Task.objects.filter(topic=topic,
                                                                                grade_difficulty=grade_difficulty,
                                                                                is_training=True).distinct().order_by('?'))[:num_topics[(topic, grade_difficulty)]]
    tasks = []
    for test_task in test_tasks:
        topic = test_task[0]
        grade_difficulty = test_task[1]
        num_topics[(topic, grade_difficulty)] -= 1
        tasks.append(get_mask_task(tasks_with_topics[(topic, grade_difficulty)][num_topics[(topic, grade_difficulty)]]))
    return tasks


def create_training_variant(student, mask_tasks, duration):
    new_variant = Variant.objects.create(student=student, is_control=False)
    if duration:
        new_variant.deadline = timezone.now() + duration
    i = 1
    for mask_task in mask_tasks:
        VariantTask(mask_task=mask_task, position=i, variant=new_variant).save()
        i += 1
    new_variant.save()
    return new_variant


@post_api_request
@login_required
@student_required
def training_test_generation(request, data):
    try:

        if 'duration' in data:
            duration = timedelta(days=data['duration']['days'],
                                 hours=data['duration']['hours'],
                                 minutes=data['duration']['minutes'])
            if duration <= timedelta(seconds=0):
                raise Error(response_code=409, text="Длительность тестирования должна быть положительной.")
        else:
            duration = None
        subject = Subject.objects.get(id=data['subject']['id'])
        grade = Grade.objects.get(id=data['grade']['id'])
        if subject.grade != grade:
            raise Error(response_code=409, text="Выбранный предмет не соответствует классу.")
        if len(data['test_tasks']) == 0:
            raise Error(response_code=409, text="Введите задачи в тест.")
        num_topics = dict()
        test_tasks = []
        for test_task in data['test_tasks']:
            topic = Topic.objects.get(id=test_task['topic']['id'])
            grade_difficulty = GradeDifficulty.objects.get(id=test_task['grade_difficulty']['id'])
            if grade != grade_difficulty.grade:
                raise Error(response_code=409, text="Выбранная сложность задачи не соответствует классу теста.")
            if subject not in topic.subjects.all():
                raise Error(response_code=409, text="Тема " + topic.name + " не соответствует предмету.")
            if (topic, grade_difficulty) in num_topics:
                num_topics[(topic, grade_difficulty)] += 1
            else:
                num_topics[(topic, grade_difficulty)] = 1
            test_tasks.append((topic, grade_difficulty))
        for topic, dif in num_topics:
            if Task.objects.filter(topic=topic, grade_difficulty=dif, is_training=True).count() < num_topics[(topic, dif)]:
                raise Error(response_code=409, text="Недостаточно тренировочных задач для темы " + topic.name + " со сложностью " + str(dif.difficulty) + ".")
        tasks = training_tasks_generation(test_tasks, num_topics)
        return create_training_variant(request.main_user.student, tasks, duration).serialize()
    except Subject.DoesNotExist:
        raise Error(response_code=409, text="Такой предмет не существует.")


@post_api_request
@login_required
@teacher_required
def create_test(request, data):
    try:
        title = data['title']
        if len(title) == 0:
            raise Error(response_code=400, text="Введите название теста.")
        if len(title) > 255:
            raise Error(response_code=400, text="Название теста не должно превышать 255 символов.")
        if 'duration' in data:
            if data['duration']['days'] > 365:
                raise Error(response_code=409, text="Количество дней в длительности не может превышать 365.")
            duration = timedelta(days=data['duration']['days'],
                                 hours=data['duration']['hours'],
                                 minutes=data['duration']['minutes'])
            if duration <= timedelta(seconds=0):
                raise Error(response_code=409, text="Длительность тестирования должна быть положительной.")
            duration_in_minutes = data['duration']['days'] * 1440 + data['duration']['hours'] * 60 + data['duration']['minutes']
        else:
            duration = None
            duration_in_minutes = None
        subject = Subject.objects.get(id=data['subject']['id'])
        if Test.objects.filter(title=title, subject=subject):
            raise Error(response_code=409, text="Тест с таким названием уже существует.")
        test_grade_difficulty = GradeDifficulty.objects.get(id=data['grade_difficulty']['id'])
        if subject.grade != test_grade_difficulty.grade:
            raise Error(response_code=409, text="Выбранный предмет не соответствует классу.")
        if len(data['test_tasks']) == 0:
            raise Error(response_code=409, text="Введите задачи в тест.")
        num_topics = dict()
        test_tasks = []
        for test_task in data['test_tasks']:
            topic = Topic.objects.get(id=test_task['topic']['id'])
            grade_difficulty = GradeDifficulty.objects.get(id=test_task['grade_difficulty']['id'])
            if test_grade_difficulty.grade != grade_difficulty.grade:
                raise Error(response_code=409, text="Выбранная сложность задачи не соответствует классу теста.")
            if subject not in topic.subjects.all():
                raise Error(response_code=409, text="Тема " + topic.name + " не соответствует предмету.")
            if (topic, grade_difficulty) in num_topics:
                num_topics[(topic, grade_difficulty)] += 1
            else:
                num_topics[(topic, grade_difficulty)] = 1
            test_tasks.append((topic, grade_difficulty))
        for topic, dif in num_topics:
            if Task.objects.filter(topic=topic, grade_difficulty=dif, is_control=True).count() < num_topics[(topic, dif)]:
                raise Error(response_code=409, 
                            text="Недостаточно задач для темы " + topic.name + " со сложностью " + str(dif.difficulty) + ".")
            if data['training_possible']:
                if Task.objects.filter(topic=topic, grade_difficulty=dif, is_training=True).count() < num_topics[(topic, dif)]:
                    raise Error(response_code=409, 
                                text="Недостаточно тренировочных задач для темы " + topic.name + " со сложностью " + str(dif.difficulty) + ".")
        new_test = Test.objects.create(title=title,
                                       subject=subject,
                                       duration=duration,
                                       duration_in_minutes=duration_in_minutes,
                                       author=request.main_user.teacher,
                                       training_possible=data['training_possible'])
        new_test.save()
        for i in range(0, len(test_tasks)):
            TestTask(topic=test_tasks[i][0], grade_difficulty=test_tasks[i][1], position=i, test=new_test).save()
        return new_test.serialize()
    except Subject.DoesNotExist:
        raise Error(response_code=409, text="Такого предмета не существует.")
    except Topic.DoesNotExist:
        raise Error(response_code=409, text="Такой темы не существует.")
    except MainUser.DoesNotExist:
        raise Error(response_code=409, text="Такого пользователя не существует.")
    except GradeDifficulty.DoesNotExist:
        raise Error(response_code=409, text="Такой сложности не существует.")
    except (ValueError, KeyError):
        raise Error(internal_code=None, response_code=404)


@post_api_request
@login_required
@teacher_required
def edit_test(request, data):
    try:
        title = data['title']
        if len(title) == 0:
            raise Error(response_code=400, text="Введите название теста.")
        if len(title) > 255:
            raise Error(response_code=400, text="Название теста не должно превышать 255 символов.")
        if 'duration' in data:
            if data['duration']['days'] > 365:
                raise Error(response_code=409, text="Количество дней в длительности не может превышать 365.")
            duration = timedelta(days=data['duration']['days'],
                                 hours=data['duration']['hours'],
                                 minutes=data['duration']['minutes'])
            if duration <= timedelta(seconds=0):
                raise Error(response_code=409, text="Длительность тестирования должна быть положительной.")
            duration_in_minutes = data['duration']['days'] * 1440 + data['duration']['hours'] * 60 + data['duration']['minutes']
        else:
            duration = None
            duration_in_minutes = None
        test = Test.objects.get(id=data['id'])
        subject = Subject.objects.get(id=data['subject']['id'])
        if title != test.title or subject != test.subject:
            if Test.objects.filter(title=title, subject=subject):
                raise Error(response_code=409, text="Тест с таким названием уже существует.")
        test_grade_difficulty = GradeDifficulty.objects.get(id=data['grade_difficulty']['id'])
        if subject.grade != test_grade_difficulty.grade:
            raise Error(response_code=409, text="Выбранный предмет не соответствует классу.")
        if len(data['test_tasks']) == 0:
            raise Error(response_code=409, text="Введите задачи в тест.")
        num_topics = dict()
        test_tasks = []
        for test_task in data['test_tasks']:
            topic = Topic.objects.get(id=test_task['topic']['id'])
            grade_difficulty = GradeDifficulty.objects.get(id=test_task['grade_difficulty']['id'])
            if test_grade_difficulty.grade != grade_difficulty.grade:
                raise Error(response_code=409, text="Выбранная сложность задачи не соответствует классу теста.")
            if subject not in topic.subjects.all():
                raise Error(response_code=409, text="Тема " + topic.name + " не соответствует предмету.")
            if (topic, grade_difficulty) in num_topics:
                num_topics[(topic, grade_difficulty)] += 1
            else:
                num_topics[(topic, grade_difficulty)] = 1
            test_tasks.append((topic, grade_difficulty))
        for topic, dif in num_topics:
            if Task.objects.filter(topic=topic, grade_difficulty=dif, is_control=True).count() < num_topics[(topic, dif)]:
                raise Error(response_code=409, 
                            text="Недостаточно задач для темы " + topic.name + " со сложностью " + str(dif.difficulty) + ".")
            if data['training_possible']:
                if Task.objects.filter(topic=topic, grade_difficulty=dif, is_training=True).count() < num_topics[(topic, dif)]:
                    raise Error(response_code=409, 
                                text="Недостаточно тренировочных задач для темы " + topic.name + " со сложностью " + str(dif.difficulty) + ".")
        test.title = title
        test.subject = subject
        test.duration = duration
        test.duration_in_minutes = duration_in_minutes
        test.grade_difficulty = test_grade_difficulty
        test.save()
        for old_test_task in TestTask.objects.filter(test=test):
            old_test_task.delete()
        for i in range(0, len(test_tasks)):
            TestTask(topic=test_tasks[i][0], grade_difficulty=test_tasks[i][1], position=i, test=test).save()
        return test.serialize()
    except Test.DoesNotExist:
        raise Error(response_code=409, text="Такого теста не существует.")
    except Subject.DoesNotExist:
        raise Error(response_code=409, text="Такого предмета не существует.")
    except Topic.DoesNotExist:
        raise Error(response_code=409, text="Такой темы не существует.")
    except MainUser.DoesNotExist:
        raise Error(response_code=409, text="Такого пользователя не существует.")
    except GradeDifficulty.DoesNotExist:
        raise Error(response_code=409, text="Такой сложности не существует.")
    except (ValueError, KeyError):
        raise Error(internal_code=None, response_code=404)


@post_api_request
@login_required
@teacher_required
def delete_test(request, data):
    try:
        test = Test.objects.get(id=data['test']['id'])
        if StudentTest.objects.filter(test=test):
            raise Error(response_code=409, text="Тест нельзя удалить, поскольку он уже назначен.")
        test.delete()
    except Test.DoesNotExist:
        raise Error(response_code=409, text="Такой тест не существует.")
    except (ValueError, KeyError):
        raise Error(internal_code=None, response_code=404)


@post_api_request
@login_required
@school_admin_required
def create_student_test(request, data):
    try:
        test = Test.objects.get(id=data['test']['id'])
        if data['starttime'] >= data['deadline']:
            raise Error(response_code=409, text="Сроки сдачи теста некорректные.")
        starttime = datetime.strptime(data['starttime'], DATETIME_FORMAT)
        deadline = datetime.strptime(data['deadline'], DATETIME_FORMAT)
        if test.duration != None:
            if test.duration > deadline - starttime:
                raise Error(response_code=409, text="Сроки сдачи теста не могут быть меньше длительности.")
        if 'training_time' in data:
            if not test.training_possible:
                raise Error(response_code=409, text="Этот тест не может быть тренировочным.")
            if data['training_time']['starttime'] >= data['training_time']['deadline']:
                raise Error(response_code=409, text="Сроки тренировки для теста некорректные.")
        students = []
        for student_id in data['students']:
            student = Student.objects.get(id=student_id['id'])
            if StudentTest.objects.filter(test=test, student=student):
                raise Error(response_code=409, text="Тест уже назначен ученику.")
            if student in students:
                raise Error(response_code=409, text="Ученики не должны повторяться.")
            else:
                students.append(student)
        for student in students:
            new_student_test = StudentTest(test=test,
                                           student=student,
                                           teacher=test.author,
                                           deadline=deadline,
                                           starttime=starttime,
                                           num_attemps=data['num_attemps'])
            if 'training_time' in data:
                new_student_test.training_deadline = datetime.strptime(data['training_time']['deadline'], DATETIME_FORMAT)
                new_student_test.training_starttime = datetime.strptime(data['training_time']['starttime'], DATETIME_FORMAT)
            new_student_test.save()
        return new_student_test.serialize()
    except Test.DoesNotExist:
        raise Error(response_code=409, text="Такой тест не существует.")
    except Student.DoesNotExist:
        raise Error(response_code=409, text="Такой ученик не существует.")


@get_api_request
@login_required
@teacher_required
def get_teacher_created_tests_list(request):
    return Test.objects.filter(author=request.main_user).serialize()


@get_api_request
@login_required
@student_required
def get_student_completed_tests_list(request):
    return StudentTest.objects.filter(student=request.main_user.student,
                                      deadline__lte=timezone.now()).serialize()


@get_api_request
@login_required
@teacher_required
def get_teacher_completed_tests_list(request):
    return StudentTest.objects.filter(teacher=request.main_user.teacher,
                                      deadline__lte=timezone.now()).serialize()


@get_api_request
@login_required
@student_required
def get_student_current_tests_list(request):
    return StudentTest.objects.filter(student=request.main_user.student,
                                      starttime__lte=timezone.now(),
                                      deadline__gt=timezone.now()).serialize()


@get_api_request
@login_required
@teacher_required
def get_teacher_current_tests_list(request):
    return StudentTest.objects.filter(teacher=request.main_user.teacher,
                                      starttime__lte=timezone.now(),
                                      deadline__gt=timezone.now()).serialize()


@get_api_request
@login_required
@teacher_required
def get_tests_list_for_student(request, student_id):
    try:
        return StudentTest.objects.filter(student=Student.objects.get(id=student_id)).serialize()
    except Student.DoesNotExist:
        raise Error(response_code=409, text="Такой ученик не существует.")


@get_api_request
@login_required
@teacher_required
def get_tests_list_for_student(request, test_id):
    try:
        return Test.objects.get(id=test_id).serialize()
    except Test.DoesNotExist:
        raise Error(response_code=409, text="Такой тест не существует.")


@get_api_request
@login_required
@student_required
def get_student_tests_list(request):
    return StudentTest.objects.filter(student=request.main_user.student).serialize()


@get_api_request
def get_variants_list(request):
    return Variant.objects.all().serialize()


@get_api_request
@login_required
@student_required
def get_variants_list_by_test(request, student_test_id):
    return Variant.objects.filter(student_test__id=student_test_id,
                                  student=request.main_user.student).serialize()


@post_api_request
@login_required
@student_required
def save_variant_task_answer(request, data):
    try:
        variant_task_ = VariantTask.objects.get(id=data['variant_task']['id'])
        if request.main_user.student.id != variant_task_.variant.student.id:
            raise Error(response_code=403, text="Вы не можете решать этот вариант.")
        now = timezone.now()
        if now > variant_task_.variant.deadline:
            raise Error(response_code=409, text="Время для написания варианта закончилось.")
        if variant_task_.variant.max_score != 0:
            raise Error(response_code=405, text="Вы уже завершили написание этого варианта.")
        question_ = Question.objects.get(id=data['question']['id'])
        answer_id = data['answer'].get('id', None)
        if answer_id is None:
            if VariantTaskAnswer.objects.filter(variant_task=variant_task_, question=question_):
                v_t_a = VariantTaskAnswer.objects.get(variant_task=variant_task_,
                                                      question=question_)
                v_t_a.text = data['answer']['text']
                v_t_a.save()
                return v_t_a.serialize()
            else:
                new_v_t_a = VariantTaskAnswer(variant_task=variant_task_,
                                              question=question_,
                                              text=data['answer']['text'])
                new_v_t_a.save()
                return new_v_t_a.serialize()
        else:
            answer_ = QuestionAnswer.objects.get(id=answer_id)
            if VariantTaskAnswer.objects.filter(variant_task=variant_task_,
                                                question=question_,
                                                answer=answer_):
                v_t_a = VariantTaskAnswer.objects.get(variant_task=variant_task_,
                                                      question=question_,
                                                      answer=answer_)
                v_t_a.delete()
                return variant_task_.serialize()
            else:
                new_v_t_a = VariantTaskAnswer(variant_task=variant_task_,
                                  question=question_,
                                  answer=answer_)
                if question_.ans_type == 1 and VariantTaskAnswer.objects.filter(variant_task=variant_task_,
                                                                                question=question_):
                    prev_ans = VariantTaskAnswer.objects.get(variant_task=variant_task_,
                                                             question=question_)
                    prev_ans.delete()
                new_v_t_a.save()
                return new_v_t_a.serialize()
    except (ValueError, KeyError):
        raise Error(response_code=404)
    except VariantTask.DoesNotExist:
        raise Error(response_code=409, text="Такого варианта не существует.")
    except Question.DoesNotExist:
        raise Error(response_code=409, text="Такого вопроса не существует.")
    except QuestionAnswer.DoesNotExist:
        raise Error(response_code=409, text="Такого ответа не существует.")


def CheckVariantAnswers(variant):
    score_ = 0
    max_score_ = 0
    set_score = set()
    for variant_task in variant.variant_tasks.all():
        max_score_ += len(variant_task.mask_task.questions.all())
        for variant_task_answer in variant_task.variant_task_answers.all():
            if variant_task_answer.answer is None:
                variant_task_answer.is_correct = (variant_task_answer.text == variant_task_answer.question.answers.all()[0].text)
            else:
                variant_task_answer.is_correct = variant_task_answer.answer.is_correct
            if not variant_task_answer.is_correct:
                set_score.add(variant_task_answer.question.id)
            variant_task_answer.save()
    score_ = max_score_ - len(set_score)
    variant.max_score = max_score_
    variant.score = score_
    variant.save()
    variant.student_test.score = max(variant.student_test.score, score_ / max_score_ * SCORE_SCALE)


@post_api_request
def check_variant_answers(request, data):
    try:
        variant = Variant.objects.get(id=data['variant']['id'])
        if variant.max_score == 0:
            CheckVariantAnswers(variant)
        return variant.serialize()
    except (ValueError, KeyError):
        raise Error(response_code=404)
    except Variant.DoesNotExist:
        raise Error(response_code=409, text="Такого варианта не существует.")
