from django.urls import path
from . import api

urlpatterns = (
    path(r'variants/list/', api.get_variants_list, name='api.tests.variants.list'),
    path(r'variants/list/<int:student_test_id>/', api.get_variants_list_by_test, name='api.tests.variants.list.by.test'),
    path(r'student_tests/list/', api.get_student_tests_list, name='api.tests.student.tests.list'),
    path(r'teacher/created_tests/list/', api.get_teacher_created_tests_list, name='api.tests.teacher.created.tests.list'),
    path(r'tests/list/<int:student_id>/', api.get_tests_list_for_student, name='api.tests.tests.list.for.student'),
    path(r'student/current_tests/list/', api.get_student_current_tests_list, name='api.tests.student.current.tests.list'),
    path(r'teacher/current_tests/list/', api.get_teacher_current_tests_list, name='api.tests.teacher.current.tests.list'),
    path(r'student/completed_tests/list/', api.get_student_completed_tests_list, name='api.tests.student.complited.tests.list'),
    path(r'teacher/completed_tests/list/', api.get_teacher_completed_tests_list, name='api.tests.teacher.complited.tests.list'),
    path(r'create_test/', api.create_test, name='api.test.create.test'),
    path(r'edit_test/', api.edit_test, name='api.test.edit.test'),
    path(r'delete_test/', api.delete_test, name='api.test.delete.test'),
    path(r'test_generation/', api.test_generation, name='api.test.test.generation'),
    path(r'training_test_generation/', api.training_test_generation, name='api.test.training.test.generation'),
    path(r'create_student_test/', api.create_student_test, name='api.test.create.student.test'),
    path(r'save_variant_task_answer/', api.save_variant_task_answer, name='api.save.variant.task.answer'),
    path(r'check_variant_answers/', api.check_variant_answers, name='api.check.variant.answer'),
)
