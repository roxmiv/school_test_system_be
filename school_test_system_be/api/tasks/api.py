from json_api.commands import Redirect, NotFoundError, Error
from json_api.decorators import get_api_request, post_api_request
from tasks.models import Subject, Topic, Task, Question, QuestionAnswer, MaskTask
from users_auth.decorators import login_required, school_admin_required, task_author_required, teacher_required
from users_auth.models import TasksAuthor, MainUser, Grade, GradeDifficulty
from tests.models import Test, TestTask, Variant, StudentTest


@get_api_request
def get_subject_name(request, subject_id):
    try:
        return Subject.objects.get(id=subject_id).serialize()
    except Subject.DoesNotExist:
        raise NotFoundError()


@get_api_request
def get_difficulty_by_grade(request, grade_id, diff):
    try:
        res = GradeDifficulty.objects.get(grade__id=grade_id, difficulty=diff)
        return res.serialize()
    except GradeDifficulty.DoesNotExist:
        raise Error(response_code=409, text="Такого класса не существует.")

@get_api_request
def get_grade_name(request, grade_id):
    try:
        return Grade.objects.get(id=grade_id).serialize()
    except Grade.DoesNotExist:
        raise NotFoundError()


@get_api_request
def get_topic_by_id(request, topic_id):
    try:
        return Topic.objects.get(id=topic_id).serialize()
    except Topic.DoesNotExist:
        raise NotFoundError()


@get_api_request
def get_task_by_id(request, task_id):
    try:
        return Task.objects.get(id=task_id).serialize()
    except Task.DoesNotExist:
        raise NotFoundError()


@get_api_request
def get_variant_by_id(request, variant_id):
    try:
        return Variant.objects.get(id=variant_id).serialize()
    except Variant.DoesNotExist:
        raise NotFoundError()


@get_api_request
def get_student_test_by_id(request, student_test_id):
    try:
        return StudentTest.objects.get(id=student_test_id).serialize()
    except StudentTest.DoesNotExist:
        raise NotFoundError()


@get_api_request
def get_test_by_id(request, test_id):
    try:
        return Test.objects.get(id=test_id).serialize()
    except Test.DoesNotExist:
        raise NotFoundError()


@get_api_request
def get_question_by_id(request, question_id):
    try:
        return Question.objects.get(id=question_id).serialize()
    except Question.DoesNotExist:
        raise NotFoundError()


@get_api_request
def get_answer_by_id(request, answer_id):
    try:
        return QuestionAnswer.objects.get(id=answer_id).serialize()
    except QuestionAnswer.DoesNotExist:
        raise NotFoundError()


@get_api_request
def get_grades_list(request):
    return Grade.objects.all().serialize()


@get_api_request
def get_variants_list(request):
    return Variant.objects.all().serialize()


@get_api_request
def get_subjects_list(request):
    return Subject.objects.all().serialize()


@get_api_request
def get_all_topics_list(request):
    return Topic.objects.all().serialize()


@get_api_request
def get_subjects_list_by_grade(request, grade_id):
    try:
        return Subject.objects.filter(grade=Grade.objects.get(id=grade_id)).serialize()
    except Grade.DoesNotExist:
        raise NotFoundError()


@get_api_request
def get_subjects_list_by_topic(request, topic_id1, topic_id2):
    try:
        topics = [topic_id1, topic_id2]
        ans = Subject.objects.all()
        for topic_id in topics:
            ans = ans.filter(topics=Topic.objects.get(id=topic_id)).distinct()
        return ans.serialize()
    except Topic.DoesNotExist:
        raise NotFoundError()


@get_api_request
def get_topics_list(request, subject_id):
    try:
        return Topic.objects.filter(subjects__in=[Subject.objects.get(id=subject_id)]).serialize()
    except Subject.DoesNotExist:
        raise NotFoundError()


@get_api_request
def get_tasks_list(request, topic_id):
    try:
        return Task.objects.filter(topic=Topic.objects.get(id=topic_id)).serialize()
    except Topic.DoesNotExist:
        raise NotFoundError()


@get_api_request
def get_grade_difficulty_list(request, grade_id):
    try:
        return GradeDifficulty.objects.filter(grade=Grade.objects.get(id=grade_id)).serialize()
    except Grade.DoesNotExist:
        raise NotFoundError()


@get_api_request
def get_student_tests_list(request):
    return StudentTest.objects.all().serialize()


@post_api_request
@login_required
@school_admin_required
def create_grade(request, data):
    try:
        if len(data['name']) == 0:
            raise Error(response_code=400, text="Введите название класса.")
        if len(data['name']) > 100:
            raise Error(response_code=400, text="Название не должно превышать 100 символов.")

        try:
            Grade.objects.get(name=data['name'])
            raise Error(response_code=409, text="Класс с таким названием уже существует.")
        except Grade.DoesNotExist:
            new_grade = Grade(name=data['name'])
            new_grade.save()
            GradeDifficulty(difficulty=0, grade=new_grade).save()
            GradeDifficulty(difficulty=1, grade=new_grade).save()
            GradeDifficulty(difficulty=2, grade=new_grade).save()
            return new_grade.serialize()
    except (ValueError, KeyError):
        raise Error(response_code=404)


@post_api_request
@login_required
@school_admin_required
def create_subject(request, data):
    try:
        if len(data['name']) == 0:
            raise Error(response_code=400, text="Введите название предмета.")
        if len(data['name']) > 100:
            raise Error(response_code=400, text="Название не должно превышать 100 символов.")
        grade = Grade.objects.get(id=data['grade']['id'])
        if Subject.objects.filter(name=data['name'], grade=grade):
            raise Error(response_code=409, text="Предмет с таким названием и классом уже существует.")
        return Subject.objects.create(name=data['name'], grade=grade).serialize()
    except Grade.DoesNotExist:
        raise Error(response_code=409, text="Такого класса не существует.")
    except (ValueError, KeyError):
        raise Error(response_code=404)


@post_api_request
@login_required
@teacher_required
def create_topic(request, data):
    try:
        if len(data['name']) == 0:
            raise Error(response_code=400, text="Введите название темы.")
        if len(data['name']) > 100:
            raise Error(response_code=400, text="Название не должно превышать 100 символов.")
        subjects = []
        for subject in data['subjects']:
            subjects.append(Subject.objects.get(id=subject['id']))
        if Topic.objects.filter(name=data['name'], subjects__in=subjects):
            raise Error(response_code=409, text="Тема с названием уже существует в каком-то предмете.")
        new_topic = Topic.objects.create(name=data['name'])
        new_topic.subjects.set(subjects)
        return new_topic.serialize()
    except Subject.DoesNotExist:
        raise Error(response_code=409, text="Такого предмета не существует.")
    except (ValueError, KeyError):
        raise Error(response_code=404)


@post_api_request
@login_required
@teacher_required
def edit_topic(request, data):
    try:
        topic = Topic.objects.get(id=data['id'])
        if len(data['name']) == 0:
            raise Error(response_code=400, text="Введите название темы.")
        if len(data['name']) > 100:
            raise Error(response_code=400, text="Название не должно превышать 100 символов.")
        subjects = []
        new_subjects = []
        old_subjects = []
        for subject in data['subjects']:
            new_subject = Subject.objects.get(id=subject['id'])
            subjects.append(new_subject)
            if new_subject not in topic.subjects.all():
                new_subjects.append(new_subject)
        for subject in topic.subjects.all():
            if subject not in subjects:
                old_subjects.append(subject)
        if topic.name != data['name'] and Topic.objects.filter(name=data['name'], subjects__in=subjects):
            raise Error(response_code=409, text="Тема с названием уже существует в каком-то предмете.")
        if topic.name == data['name'] and Topic.objects.filter(name=data['name'], subjects__in=new_subjects):
            raise Error(response_code=409, text="Тема с названием уже существует в каком-то предмете.")
        for old_subject in old_subjects:
            if TestTask.objects.filter(topic=topic, test__subject=old_subject):
                raise Error(response_code=409, text="Тему нельзя убрать из предмета " + old_subject.name + ", поскольку она используется в тесте" )
        topic.subjects.set(subjects)
        topic.name = data['name']
        topic.save()
        return topic.serialize()
    except Subject.DoesNotExist:
        raise Error(response_code=409, text="Такого предмета не существует.")
    except Topic.DoesNotExist:
        raise Error(response_code=409, text="Такой темы не существует.")
    except (ValueError, KeyError):
        raise Error(response_code=404)


@post_api_request
@login_required
@task_author_required
def create_task(request, data):
    try:
        new_task = Task(text=data['text'],
                        topic=Topic.objects.get(id=data['topic']['id']),
                        author=request.main_user,
                        is_training=data['is_training'],
                        is_control=data['is_control'])

        if len(data['questions']) == 0:
            raise Error(response_code=400, text="Добавьте хотя бы 1 вопрос в задание.")
        if (not data['is_training']) and (not data['is_control']):
            raise Error(response_code=400, text="Выберите тип задания.")
        if len(data['questions']) > 1 and len(data["text"]) == 0:
            raise Error(response_code=400, text="Введите текст в групповом задании.")
        if len(data['grade_difficulty']) == 0:
            raise Error(response_code=400, text="Введите сложность задания.")
        grades = []
        for grade_difficulty in data['grade_difficulty']:
            grade = GradeDifficulty.objects.get(id=grade_difficulty['id']).grade
            if grade in grades:
                raise Error(response_code=409, text="Для одного класса не можеть быть несколько сложностей.")
            grades.append(grade)
        for i in range(0, len(data['questions'])):
            for j in range(i):
                if data['questions'][i]['text'] == data['questions'][j]['text']:
                    raise Error(response_code=409, text="Такой вопрос уже есть в задании.")
                if data['questions'][i]['position'] == data['questions'][j]['position']:
                    raise Error(response_code=409, text="Нельзя добавить два вопроса на одну позицию.")
            if len(data['questions'][i]['text']) == 0:
                raise Error(response_code=400, text="Добавьте текст вопроса номер " +
                                                    str(data['questions'][i]['position']) + ".")
            count_true_answers = 0
            if len(data['questions'][i]['answers']) == 0:
                raise Error(response_code=400, text="Введите варианты ответов.")
            if data['questions'][i]['ans_type'] == 0:
                if len(data['questions'][i]['answers'][0]['text']) == 0:
                    raise Error(response_code=400, text="Введите текст варианта ответа.")
                if len(data['questions'][i]['answers']) > 1:
                    raise Error(response_code=400, text="Возможен только 1 вариант ответа.")
                if not data['questions'][i]['answers'][0]['is_correct']:
                    raise Error(response_code=400, text="Введенный вариант ответа должен быть правильным.")
            elif data['questions'][i]['ans_type'] == 1:
                if len(data['questions'][i]['answers']) < 2:
                    raise Error(response_code=400, text="Введите хотя бы 2 варианта ответа.")
                for j in range(len(data['questions'][i]['answers'])):
                    if len(data['questions'][i]['answers'][j]['text']) == 0:
                        raise Error(response_code=400, text="Введите текст варианта ответа.")
                    if data['questions'][i]['answers'][j]['is_correct']:
                        count_true_answers += 1
                        if count_true_answers > 1:
                            raise Error(response_code=400, text="Допустим только 1 правильный вариант ответа.")
                    for k in range(j):
                        if data['questions'][i]['answers'][j]['text'] == data['questions'][i]['answers'][k]['text']:
                            raise Error(response_code=400, text="Такой вариант ответа уже есть.")
                if count_true_answers == 0:
                    raise Error(response_code=400, text="Среди введенных вариантов ответа не выбран правильный.")
            elif data['questions'][i]['ans_type'] == 2:
                if len(data['questions'][i]['answers']) < 2:
                    raise Error(response_code=400, text="Введите хотя бы 2 варианта ответа.")
                for j in range(len(data['questions'][i]['answers'])):
                    if len(data['questions'][i]['answers'][j]['text']) == 0:
                        raise Error(response_code=400, text="Введите текст варианта ответа.")
                    if data['questions'][i]['answers'][j]['is_correct']:
                        count_true_answers += 1
                    for k in range(j):
                        if data['questions'][i]['answers'][j]['text'] == data['questions'][i]['answers'][k]['text']:
                            raise Error(response_code=400, text="Такой вариант ответа уже есть.")
                if count_true_answers == 0:
                    raise Error(response_code=400, text="Среди введенных вариантов ответа не выбран правильный.")
            else:
                raise Error(response_code=400, text="Укажите валидный тип ответа.")
        new_task.save()
        for grade_difficulty in data['grade_difficulty']:
            new_grade_difficulty = GradeDifficulty.objects.get(id=grade_difficulty['id'])
            if Subject.objects.filter(topics__in=[new_task.topic], grade=new_grade_difficulty.grade):
                new_task.grade_difficulty.add(new_grade_difficulty)
            else:
                raise Error(response_code=409, text="В классе отсутствует предмет с этой темой.")
        new_task.save()
        for question in data['questions']:
            new_question = Question(text=question['text'],
                                    ans_type=question['ans_type'],
                                    task=new_task,
                                    position=question['position'])
            new_question.save()
            for answer in question['answers']:
                QuestionAnswer(text=answer['text'],
                               is_correct=answer['is_correct'],
                               question=new_question).save()
        return new_task.serialize()
    except (ValueError, KeyError):
        raise Error(response_code=404)
    except Topic.DoesNotExist:
        raise Error(response_code=409, text="Темы с таким названием не существует.")
    except MainUser.DoesNotExist:
        raise Error(response_code=409, text="Пользователя с таким именем не существует.")
    except GradeDifficulty.DoesNotExist:
        raise Error(response_code=409, text="Такой сложности не существует.")


@post_api_request
@login_required
@task_author_required
def delete_task(request, data):
    try:
        task = Task.objects.get(id=data['id'])
        count_res = len(Task.objects.filter(topic_id=task.topic.id))
        for test in Test.objects.all():
            count = 0
            for test_task in test.test_tasks.all():
                if task.topic.id == test_task.topic.id:
                    count += 1
            if count_res - 1 < count:
                raise Error(response_code=409, text="Нельзя удалить задание, так как удаление нарушает тесты.")
        task.delete()
        return 0
    except (ValueError, KeyError):
        raise Error(response_code=404)
    except Task.DoesNotExist:
        raise Error(response_code=409, text="Такого задания не существует.")


@post_api_request
@login_required
@task_author_required
def edit_task(request, data):
    try:
        task = Task.objects.get(id=data['id'])
        count_res = len(Task.objects.filter(topic_id=task.topic.id))
        new_topic = Topic.objects.get(id=data['topic']['id'])
        if task.topic.id != new_topic.id:
            for test in Test.objects.all():
                count = 0
                for test_task in test.test_tasks.all():
                    if task.topic.id == test_task.topic.id:
                        count += 1
                if count_res - 1 < count:
                    raise Error(response_code=409, text="Нельзя изменить тему задания.")
        if len(data['questions']) == 0:
            raise Error(response_code=400, text="Добавьте хотя бы 1 вопрос в задание.")
        if (not data['is_training']) and (not data['is_control']):
            raise Error(response_code=400, text="Выберите тип задания.")
        if len(data['questions']) > 1 and len(data["text"]) == 0:
            raise Error(response_code=400, text="Введите текст в групповом задании.")
        if len(data['grade_difficulty']) == 0:
            raise Error(response_code=400, text="Введите сложность задания.")
        grades = []
        for grade_difficulty in data['grade_difficulty']:
            grade = GradeDifficulty.objects.get(id=grade_difficulty['id']).grade
            if grade in grades:
                raise Error(response_code=409, text="Для одного класса не можеть быть несколько сложностей.")
            grades.append(grade)
        for i in range(0, len(data['questions'])):
            for j in range(i):
                if data['questions'][i]['text'] == data['questions'][j]['text']:
                    raise Error(response_code=409, text="Такой вопрос уже есть в задании.")
                if data['questions'][i]['position'] == data['questions'][j]['position']:
                    raise Error(response_code=409, text="Нельзя добавить два вопроса на одну позицию.")
            if len(data['questions'][i]['text']) == 0:
                raise Error(response_code=400, text="Добавьте текст вопроса номер " +
                                                    str(data['questions'][i]['position']) + ".")
            count_true_answers = 0
            if len(data['questions'][i]['answers']) == 0:
                raise Error(response_code=400, text="Введите варианты ответов.")
            if data['questions'][i]['ans_type'] == 0:
                if len(data['questions'][i]['answers'][0]['text']) == 0:
                    raise Error(response_code=400, text="Введите текст варианта ответа.")
                if len(data['questions'][i]['answers']) > 1:
                    raise Error(response_code=400, text="Возможен только 1 вариант ответа.")
                if not data['questions'][i]['answers'][0]['is_correct']:
                    raise Error(response_code=400, text="Введенный вариант ответа должен быть правильным.")
            elif data['questions'][i]['ans_type'] == 1:
                if len(data['questions'][i]['answers']) < 2:
                    raise Error(response_code=400, text="Введите хотя бы 2 варианта ответа.")
                for j in range(len(data['questions'][i]['answers'])):
                    if len(data['questions'][i]['answers'][j]['text']) == 0:
                        raise Error(response_code=400, text="Введите текст варианта ответа.")
                    if data['questions'][i]['answers'][j]['is_correct']:
                        count_true_answers += 1
                        if count_true_answers > 1:
                            raise Error(response_code=400, text="Допустим только 1 правильный вариант ответа.")
                    for k in range(j):
                        if data['questions'][i]['answers'][j]['text'] == data['questions'][i]['answers'][k]['text']:
                            raise Error(response_code=400, text="Такой вариант ответа уже есть.")
                if count_true_answers == 0:
                    raise Error(response_code=400, text="Среди введенных вариантов ответа не выбран правильный.")
            elif data['questions'][i]['ans_type'] == 2:
                if len(data['questions'][i]['answers']) < 2:
                    raise Error(response_code=400, text="Введите хотя бы 2 варианта ответа.")
                for j in range(len(data['questions'][i]['answers'])):
                    if len(data['questions'][i]['answers'][j]['text']) == 0:
                        raise Error(response_code=400, text="Введите текст варианта ответа.")
                    if data['questions'][i]['answers'][j]['is_correct']:
                        count_true_answers += 1
                    for k in range(j):
                        if data['questions'][i]['answers'][j]['text'] == data['questions'][i]['answers'][k]['text']:
                            raise Error(response_code=400, text="Такой вариант ответа уже есть.")
                if count_true_answers == 0:
                    raise Error(response_code=400, text="Среди введенных вариантов ответа не выбран правильный.")
            else:
                raise Error(response_code=400, text="Укажите валидный тип ответа.")
        task.text = data['text']
        task.topic = new_topic
        task.is_training = data['is_training']
        task.is_control = data['is_control']
        grade_difficulties = []
        for grade_difficulty in data['grade_difficulty']:
            new_grade_difficulty = GradeDifficulty.objects.get(id=grade_difficulty['id'])
            if Subject.objects.filter(topics__in=[new_topic], grade=new_grade_difficulty.grade):
                grade_difficulties.append(new_grade_difficulty)
            else:
                raise Error(response_code=409, text="В классе отсутствует предмет с этой темой.")
        task.grade_difficulty.set(grade_difficulties)
        task.save()
        for question in task.questions.all():
            count1 = 0
            for new_q in data['questions']:
                new_q_id = new_q.get('id', None)
                if question.id == new_q_id:
                    count1 += 1
                    for answer in question.answers.all():
                        count2 = 0
                        for _new_ans in new_q['answers']:
                            _new_ans_id = _new_ans.get('id', None)
                            if answer.id == _new_ans_id:
                                count2 += 1
                                break
                        if count2 == 0:
                            answer.delete()
            if count1 == 0:
                question.delete()
        for question in data['questions']:
            id_ = question.get('id', None)
            if id_ is not None:
                get_question = Question.objects.get(id=question['id'])
                get_question.text = question['text']
                get_question.ans_type = question['ans_type']
                get_question.task = task
                get_question.position = question['position']
                get_question.save()
                for answer in question['answers']:
                    id_2 = answer.get('id', None)
                    if id_2 is not None:
                        get_answer = QuestionAnswer.objects.get(id=answer['id'])
                        get_answer.text = answer['text']
                        get_answer.is_correct = answer['is_correct']
                        get_answer.question = get_question
                        get_answer.save()
                    else:
                        QuestionAnswer(text=answer['text'],
                                       is_correct=answer['is_correct'],
                                       question=get_question).save()
            else:
                new_question = Question(text=question['text'],
                                        ans_type=question['ans_type'],
                                        task=task,
                                        position=question['position'])
                new_question.save()
                for answer in question['answers']:
                    QuestionAnswer(text=answer['text'],
                                   is_correct=answer['is_correct'],
                                   question=new_question).save()
        return task.serialize()
    except (ValueError, KeyError):
        raise Error(response_code=404)
    except Task.DoesNotExist:
        raise Error(response_code=409, text="Такого задания не существует.")
    except Topic.DoesNotExist:
        raise Error(response_code=409, text="Темы с таким названием не существует.")
    except MainUser.DoesNotExist:
        raise Error(response_code=409, text="Пользователя с таким именем не существует.")
    except GradeDifficulty.DoesNotExist:
        raise Error(response_code=409, text="Такой сложности не существует.")
    except Question.DoesNotExist:
        raise Error(response_code=409, text="Такого вопроса не существует.")
    except QuestionAnswer.DoesNotExist:
        raise Error(response_code=409, text="Такого ответа не существует.")
