from django.urls import path, include

urlpatterns = [
    path('tasks/', include('api.tasks.urls')),
    path('tests/', include('api.tests.urls')),
    path('users_auth/', include('api.users_auth.urls')),
]