from django.urls import path
from . import api

urlpatterns = (
    path(r'create_main_user/', api.create_main_user, name='api.users_auth.create.main.user'),
    path(r'create_student/', api.create_student, name='api.users_auth.create.student'),
    path(r'create_teacher/', api.create_teacher, name='api.users_auth.create.teacher'),
    path(r'create_task_author/', api.create_task_author, name='api.users_auth.create.task.author'),
    path(r'create_school_admin/', api.create_school_admin, name='api.users_auth.create.school.admin'),
    path(r'get_user/', api.get_user, name='api.users_auth.get.user'),
    path(r'login/', api.login, name='api.users_auth.login'),
    path(r'logout/', api.logout, name='api.users_auth.logout'),
    path(r'get_subject_score/<int:student_id>/<int:subject_id>/', api.get_subject_score, name='api.users_auth.get.subject.score'),
    path(r'get_student_scores/<int:student_id>/', api.get_student_scores, name='api.users_auth.get.student.scores'),
    path(r'assignment_of_subject/', api.assignment_of_subject, name='api.users_auth.assignment.of.subject'),
    path(r'get_users_list/', api.get_users_list, name='get.users.list'),
    path(r'get_user_by_id/<int:main_user_id>/', api.get_user_by_id, name='get.user.by.id'),
    path(r'change_password_for_user/', api.change_password_for_user, name='change.password.for.user'),
    path(r'create_group/', api.create_group, name='api.users_auth.create.group'),
    path(r'groups_list/', api.get_groups_list, name='api.users_auth.get.groups.list'),
    path(r'get_grade_by_student/<int:student_id>/', api.get_grade_by_student, name='api.get.grade.by.student'),
    path(r'get_grade_by_group/<int:group_id>/', api.get_grade_by_group, name='api.get.grade.by.group'),
)
