from unicodedata import name
from django.contrib.auth.models import User
from django.contrib import auth
from json_api.commands import Error
from json_api.decorators import get_api_request, post_api_request
from tasks.models import Grade, Subject
from tests.models import StudentTest
from users_auth.decorators import login_required, school_admin_required
from users_auth.models import MainUser, SchoolAdmin, Student, Group, TasksAuthor, Teacher


@post_api_request
def create_main_user(request, data):
    try:
        username = data['username']
        password = data['password']
        first_name = data['first_name']
        last_name = data['last_name']
        if len(username) == 0:
            raise Error(response_code=400, text="Введите username.")
        if len(username) > 150:
            raise Error(response_code=400, text="Очень длинное username.")
        if len(first_name) == 0:
            raise Error(response_code=400, text="Введите имя.")
        if len(first_name) > 150:
            raise Error(response_code=400, text="Очень длинное имя.")
        if len(last_name) == 0:
            raise Error(response_code=400, text="Введите фамилию.")
        if len(last_name) > 150:
            raise Error(response_code=400, text="Очень длинная фамилия.")
        User.objects.get(username=username)
        raise Error(response_code=409, text="Пользователь с таким username уже существует")
    except User.DoesNotExist:
        new_user = User.objects.create_user(username=username, password=password, first_name=first_name, last_name=last_name)
        new_main_user = MainUser.objects.create(user=new_user, username=username, first_name=first_name, last_name=last_name)
        return new_main_user.serialize()
    except (ValueError, KeyError):
        raise Error(response_code=404)


@post_api_request
@login_required
@school_admin_required
def create_student(request, data):
    try:
        main_user = MainUser.objects.get(id=data['main_user']['id'])
        if Student.objects.filter(main_user=main_user):
            raise Error(response_code=409, text="Пользователь уже является учеником")
        new_student = Student.objects.create(main_user=main_user)
        if 'group' in data:
            group = Group.objects.get(id=data['group']['id'])
            new_student.group = group
            new_student.save()
        return new_student.serialize()
    except MainUser.DoesNotExist:
        raise Error(response_code=409, text="Такого пользователя не существует")
    except Group.DoesNotExist:
        raise Error(response_code=409, text="Такой группы не существует")


@post_api_request
@login_required
@school_admin_required
def create_teacher(request, data):
    try:
        main_user = MainUser.objects.get(id=data['main_user']['id'])
        if Teacher.objects.filter(main_user=main_user):
            raise Error(response_code=409, text="Пользователь уже является учителем")
        new_teacher = Teacher.objects.create(main_user=main_user)
        if 'groups' in data:
            for group in data['groups']:
                new_group = Group.objects.get(id=group['id'])
                new_group.teachers.add(new_teacher)
                new_group.save()
        return new_teacher.serialize()
    except MainUser.DoesNotExist:
        raise Error(response_code=409, text="Такого пользователя не существует")
    except Group.DoesNotExist:
        raise Error(response_code=409, text="Такой группы не существует")


@post_api_request
def create_task_author(request, data):
    try:
        main_user = MainUser.objects.get(id=data['main_user']['id'])
        if TasksAuthor.objects.filter(main_user=main_user):
            raise Error(response_code=409, text="Пользователь уже является создателем задач")
        new_task_author = TasksAuthor.objects.create(main_user=main_user)
        return new_task_author.serialize()
    except MainUser.DoesNotExist:
        raise Error(response_code=409, text="Такого пользователя не существует")


@post_api_request
def create_school_admin(request, data):
    try:
        main_user = MainUser.objects.get(id=data['main_user']['id'])
        if SchoolAdmin.objects.filter(main_user=main_user):
            raise Error(response_code=409, text="Пользователь уже является директором")
        new_school_admin = SchoolAdmin.objects.create(main_user=main_user)
        return new_school_admin.serialize()
    except MainUser.DoesNotExist:
        raise Error(response_code=409, text="Такого пользователя не существует")


@post_api_request
@login_required
@school_admin_required
def create_group(request, data):
    try:
        grade = Grade.objects.get(id=data['grade']['id'])
        if len(data['name']) == 0:
            raise Error(response_code=400, text="Введите название класса.")
        if len(data['name']) > 100:
            raise Error(response_code=400, text="Название не должно превышать 100 символов.")
        if Group.objects.filter(grade=grade, name=data['name']):
            raise Error(response_code=409, text="Такая группа уже существует.")
        students = []
        teachers = []
        for student_id in data['students']:
            students.append(Student.objects.get(id=student_id['id']))
        for teacher_id in data['teachers']:
            teachers.append(Teacher.objects.get(id=teacher_id['id']))
        new_group = Group.objects.create(name=data['name'], grade=grade)
        for student in students:
            student.group = new_group
            student.save()
        new_group.teachers.set(teachers)
        new_group.save()
        return new_group.serialize()
    except Grade.DoesNotExist:
        raise Error(response_code=409, text="Такого класса не существует.")
    except Student.DoesNotExist:
        raise Error(response_code=409, text="Такого ученика не существует.")
    except Teacher.DoesNotExist:
        raise Error(response_code=409, text="Такого учителя не существует.")


@get_api_request
def get_groups_list(request):
    return Group.objects.all().serialize()


def GetStudentGrade(student_id):
    try:
        student = Student.objects.get(id=student_id)
        if student.group is None:
            raise Error(response_code=409, text="Ученик еще не зачислен в группу")
        else:
            grade = Grade.objects.get(name=student.group.level)
            return grade
    except Student.DoesNotExist:
        raise Error(response_code=409, text="Такого ученика не существует")
    except Grade.DoesNotExist:
        raise Error(response_code=405, text="Нет уровня, соответствующего группе ученика")


def GetGroupGrade(group_id):
    try:
        group = Group.objects.get(id=group_id)
        grade = Grade.objects.get(name=group.level)
        return grade
    except Group.DoesNotExist:
        raise Error(response_code=409, text="Такой группы не существует")
    except Grade.DoesNotExist:
        raise Error(response_code=405, text="Нет уровня, соответствующего группе")


@get_api_request
def get_grade_by_group(request, group_id):
    return GetGroupGrade(group_id).serialize()


@get_api_request
def get_grade_by_student(request, student_id):
    return GetStudentGrade(student_id).serialize()


@post_api_request
def login(request, data):
    username = data['username']
    password = data['password']
    user = auth.authenticate(username=username, password=password)
    if user is not None:
        auth.login(request, user)
        return "Вы успешно вошли в систему."
    else:
        raise Error(response_code=409, text="Ошибка авторизации")


@post_api_request
@login_required
def logout(request, data):
    if request.user.is_authenticated:
        auth.logout(request)
        return "Вы успешно вышли из системы."
    else:
        raise Error(response_code=409, text="Пользователь не авторизован")


@get_api_request
@login_required
def get_user(request):
    if request.user.is_authenticated:
        return request.main_user.serialize()
    else:
        raise Error(response_code=409, text="Пользователь не авторизован")


@get_api_request
def get_student_scores(request, student_id):
    try:
        response = []
        for subject in Student.objects.get(id=student_id).subjects.all():
            score = 0
            num = 0
            for test in StudentTest.objects.filter(student=Student.objects.get(id=student_id),
                                                   test__subject=subject):
                score += test.score
                num += 1
            response.append({'avg_score': round(score / max(num, 1), 2), 'subject': subject.name})
        return response
    except Student.DoesNotExist:
        raise Error(response_code=409, text="Такого стедента не существует")


@get_api_request
def get_subject_score(request, student_id, subject_id):
    try:
        tests = StudentTest.objects.filter(student=Student.objects.get(id=student_id),
                                           test__subject=Subject.objects.get(id=subject_id))
        score = 0
        for test in tests:
            score += test.score
        return {'avg_score': round(score / max(len(tests), 1), 2), 'tests': tests.serialize('test__title', 'score')}
    except Student.DoesNotExist:
        raise Error(response_code=409, text="Такого стедента не существует")
    except Subject.DoesNotExist:
        raise Error(response_code=409, text="Такого предмета не существует")


@post_api_request
def assignment_of_subject(request, data):
    try:
        subject = Subject.objects.get(id=data['subject']['id'])
        for student_ in data['students']:
            subject.students.add(Student.objects.get(id=student_['id']))
        return subject.serialize()
    except Student.DoesNotExist:
        raise Error(response_code=409, text="Такого стедента не существует")
    except Subject.DoesNotExist:
        raise Error(response_code=409, text="Такого предмета не существует")


@get_api_request
@login_required
@school_admin_required
def get_users_list(requset):
    return MainUser.objects.all().serialize()


@get_api_request
@login_required
@school_admin_required
def get_user_by_id(requset, main_user_id):
    try:
        return MainUser.objects.get(id=main_user_id).serialize()
    except MainUser.DoesNotExist:
        raise Error(response_code=409, text="Такого пользователя не существует.")


@post_api_request
@login_required
@school_admin_required
def change_password_for_user(requset, data):
    try:
        user = MainUser.objects.get(username=data['username']).user
        user.set_password(data['new_password'])
        user.save()
        return "Пароль был успешно изменен."
    except MainUser.DoesNotExist:
        raise Error(response_code=409, text="Такого пользователя не существует.")

