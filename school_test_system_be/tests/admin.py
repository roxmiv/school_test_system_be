from django.contrib import admin
from .models import *


admin.site.register(Test)
admin.site.register(TestTask)
admin.site.register(Variant)
admin.site.register(VariantTask)
admin.site.register(StudentTest)
admin.site.register(VariantTaskAnswer)
