from datetime import timedelta
from django.db import models
from users_auth.models import MainUser, Student, Teacher
from tasks.models import GradeDifficulty, Subject, Topic, MaskTask, Question, QuestionAnswer
from serializable.models import Serializable


class Test(Serializable):
    serialized_attributes = (
        'id',
        'title',
        'grade_difficulty',
        'subject',
        'author',
        'test_tasks',
        'training_possible',
        'duration',
        'duration_in_minutes',
    )

    title = models.CharField(max_length=255,
                             null=False,
                             blank=False,
                             default="")

    grade_difficulty = models.ForeignKey(GradeDifficulty,
                                         null=True,
                                         blank=True,
                                         related_query_name='tests',
                                         related_name='tests',
                                         on_delete=models.SET_NULL)

    subject = models.ForeignKey(Subject,
                                null=False,
                                blank=False,
                                related_query_name='tests',
                                related_name='tests',
                                on_delete=models.CASCADE)

    author = models.ForeignKey(Teacher,
                               null=False,
                               blank=False,
                               related_query_name='tests',
                               related_name='tests',
                               on_delete=models.CASCADE)

    training_possible = models.BooleanField(null=False,
                                            blank=False,
                                            default=False)
    
    duration = models.DurationField(null=True,
                                    blank=True)
    
    duration_in_minutes = models.PositiveIntegerField(null=True,
                                                      blank=True)

    def __str__(self):
        return self.title


class TestTask(Serializable):
    serialized_attributes = (
        'id',
        'topic',
        'position',
        'grade_difficulty',
    )

    class Meta:
        unique_together = (('position', 'test'),)

    topic = models.ForeignKey(Topic,
                              related_name='test_tasks',
                              related_query_name='test_tasks',
                              on_delete=models.CASCADE)
    
    grade_difficulty = models.ForeignKey(GradeDifficulty,
                                         null=True,
                                         blank=True,
                                         related_query_name='test_tasks',
                                         related_name='test_tasks',
                                         on_delete=models.SET_NULL)

    position = models.PositiveIntegerField(null=False,
                                           blank=False)

    test = models.ForeignKey(Test,
                             related_name='test_tasks',
                             related_query_name='test_tasks',
                             null=False,
                             blank=False,
                             on_delete=models.CASCADE)

    def __str__(self):
        return "%s: %d" % (self.test, self.position)


class StudentTest(Serializable):
    serialized_attributes = (
        'id',
        'student',
        'teacher',
        'starttime',
        'deadline',
        'training_starttime',
        'training_deadline',
        'num_attemps',
        'test',
        'score',
    )

    starttime = models.DateTimeField(null=False,
                                     blank=False)

    deadline = models.DateTimeField(null=False,
                                    blank=False)

    training_starttime = models.DateTimeField(null=True,
                                              blank=True)

    training_deadline = models.DateTimeField(null=True,
                                             blank=True)

    num_attemps = models.IntegerField(null=False,
                                      blank=False)

    student = models.ForeignKey(Student,
                                null=False,
                                blank=False,
                                related_query_name='student_tests',
                                related_name='student_tests',
                                on_delete=models.CASCADE)
    
    teacher = models.ForeignKey(Teacher,
                                null=True,
                                blank=True,
                                related_query_name='student_tests',
                                related_name='student_tests',
                                on_delete=models.SET_NULL)

    test = models.ForeignKey(Test,
                             null=True,
                             blank=True,
                             related_query_name='student_tests',
                             related_name='student_tests',
                             on_delete=models.SET_NULL)
    
    score = models.FloatField(null=True,
                              blank=True,
                              default=0)

    def __str__(self):
        return "%s" % (self.test)


class Variant(Serializable):
    serialized_attributes = (
        'id',
        'is_control',
        'student',
        'student_test',
        'deadline',
        'variant_tasks',
        'score',
        'max_score',
    )

    deadline = models.DateTimeField(null=True,
                                    blank=True)

    student = models.ForeignKey(Student,
                                null=False,
                                blank=False,
                                related_query_name='variants',
                                related_name='variants',
                                on_delete=models.CASCADE)
            
    student_test = models.ForeignKey(StudentTest,
                                     null=True,
                                     blank=True,
                                     related_query_name='variants',
                                     related_name='variants',
                                     on_delete=models.CASCADE)
    
    is_control = models.BooleanField(null=False,
                                     blank=False,
                                     default=False)

    score = models.PositiveIntegerField(null=True,
                                        blank=True,
                                        default=0)

    max_score = models.PositiveIntegerField(null=True,
                                            blank=True,
                                            default=0)

    def __str__(self):
        return "%s" % (self.student_test)


class VariantTask(Serializable):
    serialized_attributes = (
        'id',
        'mask_task',
        'position'
    )

    class Meta:
        unique_together = (('position', 'variant'),)

    mask_task = models.ForeignKey(MaskTask,
                                  related_name='variant_tasks',
                                  related_query_name='variant_tasks',
                                  on_delete=models.CASCADE)

    position = models.PositiveIntegerField(null=False,
                                           blank=False)

    variant = models.ForeignKey(Variant,
                                related_name='variant_tasks',
                                related_query_name='variant_tasks',
                                null=False,
                                blank=False,
                                on_delete=models.CASCADE)

    def __str__(self):
        return "%s: %d" % (self.variant, self.position)


class VariantTaskAnswer(Serializable):
    serialized_attributes = (
        'id',
        'variant_task',
        'question',
        'answer'
    )

    variant_task = models.ForeignKey(VariantTask,
                                     related_name='variant_task_answers',
                                     related_query_name='variant_task_answers',
                                     on_delete=models.CASCADE)

    question = models.ForeignKey(Question,
                                 related_name='variant_task_answers',
                                 related_query_name='variant_task_answers',
                                 on_delete=models.CASCADE)

    answer = models.ForeignKey(QuestionAnswer,
                               null=True,
                               blank=True,
                               related_name='variant_task_answers',
                               related_query_name='variant_task_answers',
                               on_delete=models.CASCADE)

    is_correct = models.BooleanField(null=True,
                                     blank=True)

    text = models.TextField(null=False,
                            blank=False,
                            default="")
