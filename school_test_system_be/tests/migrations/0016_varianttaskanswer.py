# Generated by Django 4.0.1 on 2022-03-23 14:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0007_masktask_date_time_masktask_is_final_task_date_time'),
        ('tests', '0015_studenttest_starttime'),
    ]

    operations = [
        migrations.CreateModel(
            name='VariantTaskAnswer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(default='')),
                ('answer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='variant_task_answers', related_query_name='variant_task_answers', to='tasks.questionanswer')),
                ('question', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='variant_task_answers', related_query_name='variant_task_answers', to='tasks.question')),
                ('variant_task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='variant_task_answers', related_query_name='variant_task_answers', to='tests.varianttask')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
