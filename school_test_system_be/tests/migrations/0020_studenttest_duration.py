# Generated by Django 4.0.1 on 2022-05-04 18:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tests', '0019_variant_max_score_variant_score'),
    ]

    operations = [
        migrations.AddField(
            model_name='studenttest',
            name='duration',
            field=models.DurationField(blank=True, null=True),
        ),
    ]
