# Generated by Django 4.0.1 on 2022-03-02 18:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tests', '0004_alter_variant_author_alter_variant_test'),
    ]

    operations = [
        migrations.AddField(
            model_name='test',
            name='is_control',
            field=models.BooleanField(default=False),
        ),
    ]
