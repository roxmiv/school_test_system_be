# Generated by Django 4.0.1 on 2022-03-08 11:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tests', '0005_test_is_control'),
    ]

    operations = [
        migrations.AlterField(
            model_name='testtask',
            name='test',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='test_tasks', related_query_name='test_tasks', to='tests.test'),
        ),
    ]
