# Generated by Django 4.0.1 on 2022-03-01 10:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('users_auth', '0001_initial'),
        ('tasks', '0004_remove_subject_grade'),
        ('tests', '0002_alter_test_author'),
    ]

    operations = [
        migrations.CreateModel(
            name='Variant',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='variant', related_query_name='variant', to='users_auth.mainuser')),
                ('test', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='variant', related_query_name='variant', to='tests.test')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='VariantTask',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('position', models.PositiveIntegerField()),
                ('task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='variant_tasks', related_query_name='variant_tasks', to='tasks.task')),
                ('variant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tests.variant')),
            ],
            options={
                'unique_together': {('position', 'variant')},
            },
        ),
    ]
