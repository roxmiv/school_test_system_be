# Generated by Django 4.0.1 on 2022-05-23 19:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users_auth', '0005_grade_gradedifficulty'),
        ('tests', '0026_test_training_possible'),
    ]

    operations = [
        migrations.AddField(
            model_name='testtask',
            name='grade_difficulty',
            field=models.ManyToManyField(related_name='test_tasks', related_query_name='test_tasks', to='users_auth.GradeDifficulty'),
        ),
        migrations.AlterField(
            model_name='test',
            name='grade_difficulty',
            field=models.ManyToManyField(related_name='tests', related_query_name='tests', to='users_auth.GradeDifficulty'),
        ),
    ]
