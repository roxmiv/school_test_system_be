from django.db import models
from django.contrib.auth.models import User
from serializable.models import Serializable


class MainUser(Serializable):
    serialized_attributes = (
        'id',
        'username',
        'first_name',
        'last_name',
        'is_student',
        'is_teacher',
        'is_task_author',
        'is_school_admin',
    )

    username = models.CharField(max_length=100,
                                null=False,
                                blank=False,
                                default='0')
    
    first_name = models.CharField(max_length=100,
                                  null=False,
                                  blank=False,
                                  default='0')
    
    last_name = models.CharField(max_length=100,
                                 null=False,
                                 blank=False,
                                 default='0')

    user = models.OneToOneField(User,
                                null=False,
                                blank=False,
                                related_query_name='main_user',
                                related_name='main_user',
                                on_delete=models.CASCADE)

    @property
    def is_student(self):
        return hasattr(self, 'student')

    @property
    def is_teacher(self):
        return hasattr(self, 'teacher')

    @property
    def is_school_admin(self):
        return hasattr(self, 'school_admin')

    @property
    def is_task_author(self):
        return hasattr(self, 'task_author')

    def __str__(self):
        return self.username


class Grade(Serializable):
    serialized_attributes = (
        'id',
        'name',
    )

    class Meta:
        ordering = ('name',)

    name = models.CharField(null=False,
                            blank=False,
                            unique=True,
                            max_length=200)

    def __str__(self):
        return self.name


class GradeDifficulty(Serializable):
    serialized_attributes = (
        'id',
        'difficulty',
        'grade'
    )

    class Difficulty:
        EASY = 0
        MEDIUM = 1
        HARD = 2

    DIFFICULTY_CHOICES = ((Difficulty.EASY, 'easy'),
                          (Difficulty.MEDIUM, 'medium'),
                          (Difficulty.HARD, 'hard'))

    difficulty = models.IntegerField(null=False,
                                     blank=False,
                                     choices=DIFFICULTY_CHOICES)

    grade = models.ForeignKey(Grade,
                              null=False,
                              blank=False,
                              related_query_name='grade_difficulty',
                              related_name='grade_difficulty',
                              on_delete=models.CASCADE)


class Group(Serializable):
    serialized_attributes = (
        'id',
        'grade',
        'name',
        'students',
        'teachers',
    )
    
    grade = models.ForeignKey(Grade,
                              null=True,
                              blank=True,
                              related_name='groups',
                              related_query_name='groups',
                              on_delete=models.SET_NULL)

    name = models.CharField(max_length=100,
                            null=False,
                            blank=False)

    def __str__(self):
        return "%s%s" % (self.grade, self.name)


class Teacher(Serializable):
    serialized_attributes = (
        'id',
        'main_user',
    )

    main_user = models.OneToOneField(MainUser,
                                     null=False,
                                     blank=False,
                                     related_query_name='teacher',
                                     related_name='teacher',
                                     on_delete=models.CASCADE)

    groups = models.ManyToManyField(Group,
                                    related_query_name='teachers',
                                    related_name='teachers')

    def __str__(self):
        return str(self.main_user)


class Student(Serializable):
    serialized_attributes = (
        'id',
        'main_user',
    )

    main_user = models.OneToOneField(MainUser,
                                     null=False,
                                     blank=False,
                                     related_query_name='student',
                                     related_name='student',
                                     on_delete=models.CASCADE)

    group = models.ForeignKey(Group,
                              null=True,
                              blank=True,
                              related_query_name='students',
                              related_name='students',
                              on_delete=models.SET_NULL)

    def __str__(self):
        return str(self.main_user)


class SchoolAdmin(Serializable):
    serialized_attributes = (
        'id',
        'main_user',
    )

    main_user = models.OneToOneField(MainUser,
                                     null=False,
                                     blank=False,
                                     related_query_name='school_admin',
                                     related_name='school_admin',
                                     on_delete=models.CASCADE)

    def __str__(self):
        return str(self.main_user)


class TasksAuthor(Serializable):
    serialized_attributes = (
        'id',
        'main_user',
    )

    main_user = models.OneToOneField(MainUser,
                                     null=False,
                                     blank=False,
                                     related_query_name='task_author',
                                     related_name='task_author',
                                     on_delete=models.CASCADE)

    def __str__(self):
        return str(self.main_user)
