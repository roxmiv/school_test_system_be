from django.apps import AppConfig


class MyUsersConfig(AppConfig):
    name = 'users_auth'
