from django.contrib import admin
from .models import *


admin.site.register(MainUser)
admin.site.register(Teacher)
admin.site.register(Student)
admin.site.register(SchoolAdmin)
admin.site.register(TasksAuthor)
admin.site.register(Grade)
admin.site.register(GradeDifficulty)
admin.site.register(Group)

