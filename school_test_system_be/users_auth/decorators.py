from functools import wraps
from json_api.commands import Error


def login_required(view):
    def _decorator(request, *args, **kwargs):
        if not request.user.is_authenticated:
            raise Error(response_code=409, text="Пользователь не авторизован")
        if not request.user.is_active:
            raise Error(response_code=409, text="Пользователь отключен")
        setattr(request, 'main_user', request.user.main_user)
        return view(request, *args, **kwargs)
    return wraps(view)(_decorator)


def student_required(view):
    def _decorator(request, *args, **kwargs):
        if request.main_user.is_student:
            return view(request, *args, **kwargs)
        else:
            raise Error(response_code=403, text="У вас недостаточно прав для выполнения данного действия.")
    return wraps(view)(_decorator)


def teacher_required(view):
    def _decorator(request, *args, **kwargs):
        if request.main_user.is_teacher:
            return view(request, *args, **kwargs)
        else:
            raise Error(response_code=403, text="У вас недостаточно прав для выполнения данного действия.")
    return wraps(view)(_decorator)


def school_admin_required(view):
    def _decorator(request, *args, **kwargs):
        if request.main_user.is_school_admin:
            return view(request, *args, **kwargs)
        else:
            raise Error(response_code=403, text="У вас недостаточно прав для выполнения данного действия.")
    return wraps(view)(_decorator)


def task_author_required(view):
    def _decorator(request, *args, **kwargs):
        if request.main_user.is_task_author or request.main_user.is_teacher:
            return view(request, *args, **kwargs)
        else:
            raise Error(response_code=403, text="У вас недостаточно прав для выполнения данного действия.")
    return wraps(view)(_decorator)