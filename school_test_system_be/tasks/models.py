from django.db import models
from users_auth.models import Student, Teacher, MainUser, Grade, GradeDifficulty
from serializable.models import Serializable


class Subject(Serializable):
    serialized_attributes = (
        'id',
        'name',
        'grade',
        'teachers',
        'students',
    )

    class Meta:
        ordering = ('name',)

    name = models.CharField(max_length=100,
                            null=False,
                            blank=False)

    grade = models.ForeignKey(Grade,
                              null=True,
                              blank=True,
                              related_query_name='subjects',
                              related_name='subjects',
                              on_delete=models.SET_NULL)

    teachers = models.ManyToManyField(Teacher,
                                      related_query_name='subjects',
                                      related_name='subjects')

    students = models.ManyToManyField(Student,
                                      related_query_name='subjects',
                                      related_name='subjects')

    def __str__(self):
        return self.name


class Topic(Serializable):
    serialized_attributes = (
        'id',
        'name',
        'subjects'
    )

    class Meta:
        ordering = ('name', )

    name = models.CharField(max_length=100,
                            null=False,
                            blank=False)

    subjects = models.ManyToManyField(Subject,
                                      related_query_name='topics',
                                      related_name='topics')


class Task(Serializable):
    serialized_attributes = (
        'id',
        'text',
        'grade_difficulty',
        'topic',
        'author',
        'is_training',
        'is_control',
        'questions',
        'date_time'
    )

    text = models.TextField(null=False,
                            blank=False,
                            default="")

    grade_difficulty = models.ManyToManyField(GradeDifficulty,
                                              related_query_name='tasks',
                                              related_name='tasks')

    topic = models.ForeignKey(Topic,
                              null=False,
                              blank=False,
                              related_query_name='tasks',
                              related_name='tasks',
                              on_delete=models.CASCADE)

    author = models.ForeignKey(MainUser,
                               null=False,
                               blank=False,
                               related_query_name='tasks',
                               related_name='tasks',
                               on_delete=models.CASCADE)

    is_training = models.BooleanField(null=False,
                                      blank=False,
                                      default=True)

    is_control = models.BooleanField(null=False,
                                     blank=False,
                                     default=False)

    date_time = models.DateTimeField(auto_now=True)


class MaskTask(Serializable):
    serialized_attributes = (
        'id',
        'text',
        'grade_difficulty',
        'topic',
        'author',
        'is_training',
        'is_control',
        'questions',
        'date_time',
        'task',
        'is_final'
    )

    text = models.TextField(null=False,
                            blank=False,
                            default="")

    task = models.ForeignKey(Task,
                             null=True,
                             blank=True,
                             related_name='mask_tasks',
                             related_query_name='mask_tasks',
                             on_delete=models.SET_NULL)

    grade_difficulty = models.ManyToManyField(GradeDifficulty,
                                              related_query_name='mask_tasks',
                                              related_name='mask_tasks')

    topic = models.ForeignKey(Topic,
                              null=False,
                              blank=False,
                              related_query_name='mask_tasks',
                              related_name='mask_tasks',
                              on_delete=models.CASCADE)

    author = models.ForeignKey(MainUser,
                               null=True,
                               blank=True,
                               related_query_name='mask_tasks',
                               related_name='mask_tasks',
                               on_delete=models.SET_NULL)

    is_training = models.BooleanField(null=False,
                                      blank=False,
                                      default=True)

    is_control = models.BooleanField(null=False,
                                     blank=False,
                                     default=False)

    date_time = models.DateTimeField(auto_now=True)

    is_final = models.BooleanField(null=False,
                                   blank=False,
                                   default=False)


class Question(Serializable):
    serialized_attributes = (
        'id',
        'text',
        'ans_type',
        'answers',
        'position'
    )

    class AnswersType:
        ACCURATE = 0
        SELECT = 1
        MULTISELECT = 2

    ANSWERS_TYPE_CHOICES = ((AnswersType.ACCURATE, 'accurate'),
                            (AnswersType.SELECT, 'select'),
                            (AnswersType.MULTISELECT, 'multiselect'))

    text = models.TextField(null=True,
                            blank=True,
                            default="")

    ans_type = models.IntegerField(null=False,
                                   blank=False,
                                   choices=ANSWERS_TYPE_CHOICES)

    task = models.ForeignKey(Task,
                             null=True,
                             blank=True,
                             related_query_name='questions',
                             related_name='questions',
                             on_delete=models.SET_NULL)

    mask_task = models.ForeignKey(MaskTask,
                                  null=True,
                                  blank=True,
                                  related_query_name='questions',
                                  related_name='questions',
                                  on_delete=models.SET_NULL)

    position = models.PositiveIntegerField(null=False,
                                           blank=False)


class QuestionAnswer(Serializable):
    serialized_attributes = (
        'id',
        'text',
        'is_correct'
    )

    question = models.ForeignKey(Question,
                                 null=False,
                                 blank=False,
                                 related_name='answers',
                                 related_query_name='answers',
                                 on_delete=models.CASCADE)

    text = models.TextField(null=False,
                            blank=False,
                            default="")

    is_correct = models.BooleanField(null=False,
                                     blank=False)


class Image(models.Model):

    task = models.ForeignKey(Task,
                             null=True,
                             blank=True,
                             related_query_name='images',
                             related_name='images',
                             on_delete=models.SET_NULL)

    mask_task = models.ForeignKey(MaskTask,
                                  null=True,
                                  blank=True,
                                  related_query_name='images',
                                  related_name='images',
                                  on_delete=models.SET_NULL)

    question_answer = models.ForeignKey(QuestionAnswer,
                                        null=True,
                                        blank=True,
                                        related_query_name='images',
                                        related_name='images',
                                        on_delete=models.CASCADE)

    questions = models.ForeignKey(Question,
                                  null=True,
                                  blank=True,
                                  related_query_name='images',
                                  related_name='images',
                                  on_delete=models.CASCADE)

    image = models.ImageField(upload_to='images/')


class File(models.Model):

    task = models.ForeignKey(Task,
                             null=True,
                             blank=True,
                             related_query_name='files',
                             related_name='files',
                             on_delete=models.SET_NULL)

    mask_task = models.ForeignKey(MaskTask,
                                  null=True,
                                  blank=True,
                                  related_query_name='files',
                                  related_name='files',
                                  on_delete=models.SET_NULL)

    task_answer = models.ForeignKey(QuestionAnswer,
                                    null=True,
                                    blank=True,
                                    related_query_name='files',
                                    related_name='files',
                                    on_delete=models.CASCADE)

    questions = models.ForeignKey(Question,
                                  null=True,
                                  blank=True,
                                  related_query_name='files',
                                  related_name='files',
                                  on_delete=models.CASCADE)

    file = models.FileField(upload_to='files/')
