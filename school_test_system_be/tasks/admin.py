from django.contrib import admin
from .models import *


admin.site.register(Subject)
admin.site.register(Topic)
admin.site.register(Task)
admin.site.register(Question)
admin.site.register(QuestionAnswer)
admin.site.register(Image)
admin.site.register(File)
admin.site.register(MaskTask)
