# Generated by Django 4.0.1 on 2022-03-14 20:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0006_alter_file_task_alter_image_task_alter_question_task_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='masktask',
            name='date_time',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='masktask',
            name='is_final',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='task',
            name='date_time',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
