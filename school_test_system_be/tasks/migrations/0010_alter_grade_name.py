# Generated by Django 4.0.1 on 2022-04-20 12:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0009_alter_subject_grade'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grade',
            name='name',
            field=models.PositiveIntegerField(unique=True),
        ),
    ]
