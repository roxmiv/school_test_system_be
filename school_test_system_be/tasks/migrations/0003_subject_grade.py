# Generated by Django 4.0.1 on 2022-02-19 22:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tasks', '0002_alter_task_author'),
    ]

    operations = [
        migrations.AddField(
            model_name='subject',
            name='grade',
            field=models.ManyToManyField(related_name='subjects', related_query_name='subjects', to='tasks.Grade'),
        ),
    ]
